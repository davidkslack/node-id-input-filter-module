Node ID input filter module
---------------------------

This is a module for Drupal 6 to add a new node filter.

The filter will alow a user, when creating / editing content, to add some markup like [#1234] with an ID number and this will be exchanged for a link to the node and the title or a CONTENT DELETED if its not found.

As well as this the link will have a class strikeThrough if there is a case and the case is closed.

This module was writtern by me (David Slack) and sponcered be Ixis  